import { createRouter, createWebHistory } from 'vue-router'
import ProductComponents from '../views/ProductComponents.vue'
import ProductPage from "../views/ProductPage.vue";

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: ProductComponents
    },
    {
      path: '/product/:slug',
      name: 'product',
      component: ProductPage
    },
    { 
      path: '/:pathMatch(.*)', 
      component: () => import('../views/NonePageView.vue') 
    }
  ]
}) 



export default router
